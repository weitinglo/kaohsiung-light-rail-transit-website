function getTodayCachePath(){
    var d = new Date();
    d.setMinutes(0);
    d.setSeconds(0);
    var hour = d.getUTCHours() + 8;
    var datePath = d.getFullYear() + "" + ("0" + (d.getMonth() + 1)).slice(-2) + "" + ("0" + d.getDate()).slice(-2);
    return "cache/" + datePath;
}
function countArea(areaDataJson, detectAreaJson) {
    for (i in detectAreaJson) {
        if (detectAreaJson[i] == 1) {
            areaDataJson[i]++;
        }
    }
    return areaDataJson;
}
function intrudeCountTotal(areaJson){
    let count = 0;
    for(k in areaJson){
        count += areaJson[k];
    }
    console.log(count);
    return count;
}
function intrudeCount(areaJson) {
    count = 0;
    if (areaJson.r1 || areaJson.r3)
        count += areaJson.r1 > areaJson.r3 ? areaJson.r1 : areaJson.r3;
    if (areaJson.r2 || areaJson.r4)
        count += areaJson.r2 > areaJson.r4 ? areaJson.r2 : areaJson.r4;
    if (areaJson.r5 || areaJson.r7)
        count += areaJson.r5 > areaJson.r7 ? areaJson.r5 : areaJson.r7;
    if (areaJson.r6 || areaJson.r8)
        count += areaJson.r6 > areaJson.r8 ? areaJson.r6 : areaJson.r8;
    if (areaJson.r9 || areaJson.r11)
        count += areaJson.r9 > areaJson.r11 ? areaJson.r9 : areaJson.r11;
    if (areaJson.r10 || areaJson.r12)
        count += areaJson.r10 > areaJson.r12 ? areaJson.r10 : areaJson.r12;
    if (areaJson.r13 || areaJson.r15 || areaJson.r51) {
        let temp = areaJson.r13 > areaJson.r15 ? areaJson.r13 : areaJson.r15;
        temp = temp > areaJson.r51 ? temp : areaJson.r51;
        count += temp;
    }
    if (areaJson.r14 || areaJson.r16 || areaJson.r52) {
        let temp = areaJson.r14 > areaJson.r16 ? areaJson.r14 : areaJson.r16;
        temp = temp > areaJson.r52 ? temp : areaJson.r52;
        count += temp;
    }
    if (areaJson.r17 || areaJson.r19)
        count += areaJson.r17 > areaJson.r19 ? areaJson.r17 : areaJson.r19;
    if (areaJson.r18 || areaJson.r20)
        count += areaJson.r18 > areaJson.r20 ? areaJson.r18 : areaJson.r20;
    if (areaJson.r21 || areaJson.r23)
        count += areaJson.r21 > areaJson.r23 ? areaJson.r21 : areaJson.r23;
    if (areaJson.r22 || areaJson.r24)
        count += areaJson.r22 > areaJson.r24 ? areaJson.r22 : areaJson.r24;
    if (areaJson.r25 || areaJson.r27)
        count += areaJson.r25 > areaJson.r27 ? areaJson.r25 : areaJson.r27;
    if (areaJson.r26 || areaJson.r28)
        count += areaJson.r26 > areaJson.r28 ? areaJson.r26 : areaJson.r28;
    if (areaJson.r29)
        count += areaJson.r29;
    if (areaJson.r30)
        count += areaJson.r30;
    if (areaJson.r31 || areaJson.r32 || areaJson.r33 || areaJson.r34 || areaJson.r35 || areaJson.r36 || areaJson.r37 || areaJson.r38 || areaJson.r39
        || areaJson.r53 || areaJson.r54 || areaJson.r55 || areaJson.r56 || areaJson.r57 || areaJson.r58 || areaJson.r59) {
        let temp = areaJson.r31 > areaJson.r32 ? areaJson.r31 : areaJson.r32;
        temp = temp > areaJson.r33 ? temp : areaJson.r33;
        temp = temp > areaJson.r34 ? temp : areaJson.r34;
        temp = temp > areaJson.r35 ? temp : areaJson.r35;
        temp = temp > areaJson.r36 ? temp : areaJson.r36;
        temp = temp > areaJson.r37 ? temp : areaJson.r37;
        temp = temp > areaJson.r38 ? temp : areaJson.r38;
        temp = temp > areaJson.r39 ? temp : areaJson.r39;
        temp = temp > areaJson.r53 ? temp : areaJson.r53;
        temp = temp > areaJson.r54 ? temp : areaJson.r54;
        temp = temp > areaJson.r55 ? temp : areaJson.r55;
        temp = temp > areaJson.r56 ? temp : areaJson.r56;
        temp = temp > areaJson.r57 ? temp : areaJson.r57;
        temp = temp > areaJson.r58 ? temp : areaJson.r58;
        temp = temp > areaJson.r59 ? temp : areaJson.r59;
        count += temp;
    }
    if (areaJson.r40)
        count += areaJson.r40;
    if (areaJson.r41 || areaJson.r61 || areaJson.r60) {
        let temp = areaJson.r41 > areaJson.r61 ? areaJson.r41 : areaJson.r61;
        temp = temp > areaJson.r60 ? temp : areaJson.r60;
        count += temp;
    }

    if (areaJson.r42 || areaJson.r62)
        count += areaJson.r42 > areaJson.r62 ? areaJson.r42 : areaJson.r62;
    if (areaJson.r43)
        count += areaJson.r43;
    if (areaJson.r44)
        count += areaJson.r44;
    if (areaJson.r45 || areaJson.r47)
        count += areaJson.r45 > areaJson.r47 ? areaJson.r45 : areaJson.r47;
    if (areaJson.r46 || areaJson.r48)
        count += areaJson.r46 > areaJson.r48 ? areaJson.r46 : areaJson.r48;
    if (areaJson.r49)
        count += areaJson.r49;
    if (areaJson.r50)
        count += areaJson.r50;
    return count;
}

exports.getTodayCachePath = getTodayCachePath;
exports.countArea = countArea;
exports.intrudeCount = intrudeCount;
exports.intrudeCountTotal = intrudeCountTotal;