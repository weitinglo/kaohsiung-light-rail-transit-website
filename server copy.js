const express = require('express')
const utility = require('./utility')
const app = express()
const fs = require('fs')
const port = 3000
var bodyParser = require('body-parser');

Stream = require('node-rtsp-stream')

stream1 = new Stream({
    name: 'name1',
    streamUrl: 'rtsp://admin:ChipER9101@210.71.208.244:7001/a594f2a1-8cbf-ea12-83c1-e79a818895bc?stream=1',
    wsPort: 9999,
    ffmpegOptions: { // options ffmpeg flags
        '-stats': '', // an option with no neccessary value uses a blank string
        '-r': 30 // options with required values specify the value after the key
    }
})
stream2 = new Stream({
    name: 'name2',
    streamUrl: 'rtsp://admin:ChipER9101@210.71.208.244:7001/ce6e993a-511e-68f3-77a5-e3eebe0eae1b?stream=1',
    wsPort: 8888,
    ffmpegOptions: { // options ffmpeg flags
        '-stats': '', // an option with no neccessary value uses a blank string
        '-r': 30 // options with required values specify the value after the key
    }
})

// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
var firebase = require("firebase/app");

// Add the Firebase products that you want to use
require("firebase/auth");
require("firebase/firestore");
require("firebase/database");

var firebaseConfig = {
    apiKey: "AIzaSyDdnhBWmyStk3DgV_l6y_iWaMrBQKAvT18",
    authDomain: "kaohsiung-lrt.firebaseapp.com",
    databaseURL: "https://kaohsiung-lrt.firebaseio.com",
    projectId: "kaohsiung-lrt",
    storageBucket: "kaohsiung-lrt.appspot.com",
    messagingSenderId: "550813548509",
    appId: "1:550813548509:web:041a79ede4881ab175c034",
    measurementId: "G-WRWZL1XRR7"
  };
  // Initialize Firebase
firebase.initializeApp(firebaseConfig);
var database = firebase.database();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(express.static(__dirname + '/public'));

app.get('/',function(req,res) {
    res.sendFile(__dirname + "/" + "index.html");
});
app.get('/index.html', function (req, res) {
    res.sendFile(__dirname + "/" + "index.html");
});

app.get('/analysis.html', function (req, res) {
    res.sendFile(__dirname + "/" + "analysis.html");
});

app.get('/live.html', function (req, res) {
    res.sendFile(__dirname + "/" + "live.html");
});

app.get('/map.html', function (req, res) {
    res.sendFile(__dirname + "/" + "map.html");
});
app.get('/map14.html', function (req, res) {
    res.sendFile(__dirname + "/" + "map14.html");
});
app.get('/lrtLiveData',function(req,res){
    var date = req.query.date;
    var starCountRef = firebase.database().ref(date + '/restriction').orderByChild('time').limitToLast(1);
    starCountRef.on('value', function (snapshot) {
        if (snapshot.val()==null){
            var json ={msg:"No data yet",code:101};
            res.send(json);
        }else
            res.send(snapshot.val());
    });
});
app.get('/lrtData',function(req,res){
    var date = req.query.date;
    let path = utility.getTodayCachePath();
    let totalDataCount = 0;
    let totalIntrudeCount = 0;
    var fileArr=[];
    var starCountRef = firebase.database().ref(date+'/cache');
    starCountRef.once('value', function (snapshot) {
        if (snapshot.val()==null){
            var json ={msg:"No data yet",code:101};
            res.send(json);
        }else{
            res.send(snapshot.val());
        }
    });
});
app.get('/lrtTotalData', function (req, res) {
    var date = req.query.date;
    var starCountRef = firebase.database().ref("accumulated");
    starCountRef.once('value', function (snapshot) {
        if (snapshot.val() == null) {
            var json = { msg: "No data yet", code: 101 };
            res.send(json);
        } else
            res.send(snapshot.val());
    });
});

var totoalIntrudeCount = 0;
function savePreDay(){
    let today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    let yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    yesterday.setHours(0);
    yesterday.setMinutes(0);
    yesterday.setSeconds(0);

    var datePath = yesterday.getFullYear() + "" + ("0" + (yesterday.getMonth() + 1)).slice(-2) + "" + ("0" + yesterday.getDate()).slice(-2);
    var starCountRef = firebase.database().ref(datePath + '/restriction').orderByChild('time').startAt(Date.parse(yesterday)).endAt(Date.parse(today));
    starCountRef.once('value', function (snapshot) {
        if (snapshot.val() == null) {
            console.log("no data");
        } else {
            console.log("has data");
            var totoalDataCount = 0;
            var jsonObj = snapshot.val();
            var areaData = {
                "r1": 0, "r2": 0, "r3": 0, "r4": 0, "r5": 0, "r6": 0, "r7": 0, "r8": 0, "r9": 0, "r10": 0,
                "r11": 0, "r12": 0, "r13": 0, "r14": 0, "r15": 0, "r16": 0, "r17": 0, "r18": 0, "r19": 0, "r20": 0,
                "r21": 0, "r22": 0, "r23": 0, "r24": 0, "r25": 0, "r26": 0, "r27": 0, "r28": 0, "r29": 0, "r30": 0,
                "r31": 0, "r32": 0, "r33": 0, "r34": 0, "r35": 0, "r36": 0, "r37": 0, "r38": 0, "r39": 0, "r40": 0,
                "r41": 0, "r42": 0, "r43": 0, "r44": 0, "r45": 0, "r46": 0, "r47": 0, "r48": 0, "r49": 0, "r50": 0,
                "r51": 0, "r52": 0, "r53": 0, "r54": 0, "r55": 0, "r56": 0, "r57": 0, "r58": 0, "r59": 0, "r60": 0,
                "r61": 0, "r62": 0, "r63": 0, "r64": 0, "r65": 0
            };
            var areaDataTotal = {};
            for (key in jsonObj) {
                var detectAreaJson = jsonObj[key];
                areaDataTotal = utility.countArea(areaData, detectAreaJson);
            }
            totoalDataCount = utility.intrudeCount(areaDataTotal);
            let jsonDataToSave = {
                "date": datePath,
                "dataCount": totoalDataCount,
                "intrudeCount": totoalIntrudeCount,
                "data": areaDataTotal
            }
            firebase.database().ref('accumulated/' + datePath).set(jsonDataToSave).then().catch();;
        }
    });

}
function savePreHour(){
    var d = new Date();
    d.setMinutes(0);
    d.setSeconds(0);
    var hour = d.getUTCHours()+8 - 1;
    var datePath = d.getFullYear() + "" + ("0" + (d.getMonth() + 1)).slice(-2) + "" + ("0" + d.getDate()).slice(-2);
    let hourTS = Date.parse(d);
    let preHourTS = hourTS - 3600000;
    totoalIntrudeCount = 0;
    var starCountRef = firebase.database().ref(datePath + '/restriction').orderByChild('time').startAt(preHourTS).endAt(hourTS);
    starCountRef.once('value', function (snapshot) {
        if (snapshot.val() == null) {
            console.log("no val");
            var json = { msg: "No data yet", code: 101 };
        } else {
            console.log("got val");
            var totoalDataCount = 0;
            var jsonObj = snapshot.val();
            var areaData = {
                "r1": 0, "r2": 0, "r3": 0, "r4": 0, "r5": 0, "r6": 0, "r7": 0, "r8": 0, "r9": 0, "r10": 0,
                "r11": 0, "r12": 0, "r13": 0, "r14": 0, "r15": 0, "r16": 0, "r17": 0, "r18": 0, "r19": 0, "r20": 0,
                "r21": 0, "r22": 0, "r23": 0, "r24": 0, "r25": 0, "r26": 0, "r27": 0, "r28": 0, "r29": 0, "r30": 0,
                "r31": 0, "r32": 0, "r33": 0, "r34": 0, "r35": 0, "r36": 0, "r37": 0, "r38": 0, "r39": 0, "r40": 0,
                "r41": 0, "r42": 0, "r43": 0, "r44": 0, "r45": 0, "r46": 0, "r47": 0, "r48": 0, "r49": 0, "r50": 0,
                "r51": 0, "r52": 0, "r53": 0, "r54": 0, "r55": 0, "r56": 0, "r57": 0, "r58": 0, "r59": 0, "r60": 0,
                "r61": 0, "r62": 0, "r63": 0, "r64": 0, "r65": 0
            };
            var areaDataTotal = {};
            for (key in jsonObj) {
                var detectAreaJson = jsonObj[key];
                areaDataTotal = utility.countArea(areaData, detectAreaJson);
            }
            totoalDataCount = utility.intrudeCount(areaDataTotal);
            let jsonDataToSave = {
                "hour": d.getUTCHours()+8 - 1,
                "dataCount": totoalDataCount,
                "intrudeCount": totoalIntrudeCount,
                "data": areaDataTotal
            }
            firebase.database().ref(datePath + '/cache/' + (d.getUTCHours()+8)).set(jsonDataToSave);
        }
    });
}

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

//savePreDay();
//updateCache();
setInterval(savePreHour, 2400000);
setInterval(savePreDay, 43200000);