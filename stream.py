
from imutils.video import VideoStream
from flask import Response
from flask import Flask
from flask import render_template
import threading
import argparse
import datetime
import imutils
import time
import cv2

app = Flask(__name__)

outputFrame = None
lock = threading.Lock()
vs = VideoStream('rtsp://admin:chiper9101@110.25.94.197:50554/profile1').start()

outputFrame2 = None
lock2 = threading.Lock()
vs2 = VideoStream('rtsp://admin:chiper9101@110.25.94.197:50555/profile1').start()


@app.route("/")
def index():
	# return the rendered template
	return render_template("index.html")

def detect_motion():
	print("starting cam 1")
	global vs, outputFrame, lock
	while True:	
		frame = vs.read()
		with lock:
			if frame is not None:
				outputFrame = frame.copy()
			else:
				vs = VideoStream('rtsp://admin:chiper9101@210.71.208.244:50554/profile2').start()
		
def generate():
	global outputFrame, lock
	while True:
		with lock:
			if outputFrame is None:
				continue
			# encode the frame in JPEG format
			(flag, encodedImage) = cv2.imencode(".jpg", outputFrame)
			if not flag:
				continue

		yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + 
			bytearray(encodedImage) + b'\r\n')

@app.route("/video_feed")
def video_feed():
	# return the response generated along with the specific media
	# type (mime type)
	return Response(generate(),
		mimetype = "multipart/x-mixed-replace; boundary=frame")
 
def detect_motion2():
	print("starting cam 2")
	global vs2, outputFrame2, lock2
	while True:	
		frame = vs2.read()
		with lock2:
			if frame is not None:
				outputFrame2 = frame.copy()
			else:
				vs2 = VideoStream('rtsp://admin:chiper9101@210.71.208.244:50555/profile2').start()

  
def generate2():
	global outputFrame2, lock2
	while True:
		with lock2:
			if outputFrame2 is None:
				continue

			# encode the frame in JPEG format
			(flag, encodedImage) = cv2.imencode(".jpg", outputFrame2)

			if not flag:
				continue

		yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + 
			bytearray(encodedImage) + b'\r\n')
  
@app.route("/video_feed2")
def video_feed2():
	# return the response generated along with the specific media
	# type (mime type)
	return Response(generate2(),
		mimetype = "multipart/x-mixed-replace; boundary=frame")
 
def checkThread(sleeptimes=60,initThreadsName=[]):
	for i in range(0,10080):
		print("checking")
		nowThreadsName=[]
		now=threading.enumerate()
		for i in now:
			nowThreadsName.append(i.getName())

		for ip in initThreadsName:
			if ip in nowThreadsName:
				pass 
			else:
				print (ip)
				if(ip=='cam1'):
					t=threading.Thread(target=detect_motion,args=())
					t.setName(ip)
					t.start()
				elif(ip=='cam2'):
					t=threading.Thread(target=detect_motion2,args=())
					t.setName(ip)
					t.start()
		time.sleep(sleeptimes)
    
# check to see if this is the main thread of execution
if __name__ == '__main__':
    threads=[]
    initThreadsName=[]
    try:
        t1 = threading.Thread(target=detect_motion, args=())
        t1.setName("cam1")
        threads.append(t1)
        t2 = threading.Thread(target=detect_motion2, args=())
        t2.setName("cam2")
        threads.append(t2)
        
        for t in threads:
            t.start()
        init=threading.enumerate()
        for i in init:
            initThreadsName.append(i.getName())
   
        # check=threading.Thread(target=checkThread,args=(60,initThreadsName))
        # check.setName('Thread:check')
        # check.start()
  
        # t2 = threading.Thread(target=detect_motion2, args=())
        # t2.daemon = True
        # t2.start()
        app.run('128.199.150.66', '8000', debug=True,
            threaded=True, use_reloader=False)
    except err:
        vs.stop()
