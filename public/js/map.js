function showAlarmOnArea(areaJson) {
    /*C13*/
   if (areaJson.r1 || areaJson.r2 || areaJson.r3){
       $("#area_2").attr("fill","red")
   } else {
       $("#area_2").attr("fill","#49c419")
   }
   if (areaJson.r4 || areaJson.r5 || areaJson.r6){
       $("#area_4").attr("fill","red")
   } else {
       $("#area_4").attr("fill","#49c419")
   } 
   if (areaJson.r7){
       $("#area_6").attr("fill","red")
   } else {
       $("#area_6").attr("fill","#49c419")
   } 
   if (areaJson.r8 || areaJson.r9){
       $("#area_8").attr("fill","red")
   } else {
       $("#area_8").attr("fill","#49c419")
   } 
   if (areaJson.r10 || areaJson.r11 || areaJson.r12){
       $("#area_10").attr("fill","red")
   } else {
       $("#area_10").attr("fill","#49c419")
   } 
   if (areaJson.r13 || areaJson.r14) {
       $("#area_12").attr("fill","red")
   } else {
       $("#area_12").attr("fill","#49c419")
   } 
   if (areaJson.r15 || areaJson.r16) {
       $("#area_14").attr("fill","red")
   } else {
       $("#area_14").attr("fill","#49c419")
   } 
   if (areaJson.r17 || areaJson.r18) {
       $("#area_16").attr("fill","red")
   } else {
       $("#area_16").attr("fill","#49c419")
   } 
   if (areaJson.r19 || areaJson.r20 || areaJson.r21 || areaJson.r22 || areaJson.r23 || areaJson.r24 || areaJson.r25 || areaJson.r26 || areaJson.r27 || areaJson.r28 || areaJson.r29 || areaJson.r30 || areaJson.r31 || areaJson.r32 || areaJson.r33) {
       $("#area_17").attr("fill","red")
   } else {
       $("#area_17").attr("fill","#49c419")
   } 
   if (areaJson.r34 || areaJson.r35) {
       $("#area_19").attr("fill","red")
   } else {
       $("#area_19").attr("fill","#49c419")
   } 
   if (areaJson.r36) {
       $("#area_20").attr("fill","red")
   } else {
       $("#area_20").attr("fill","#49c419")
   } 
   if (areaJson.r37 || areaJson.r38) {
       $("#area_22").attr("fill","red")
   } else {
       $("#area_22").attr("fill","#49c419")
   } 
   if (areaJson.r39 || areaJson.r40) {
       $("#area_24").attr("fill","red")
   } else {
       $("#area_24").attr("fill","#49c419")
   } 
   if (areaJson.r41) {
       $("#area_26").attr("fill","red")
   } else {
       $("#area_26").attr("fill","#49c419")
   } 

/*C14*/
   if (areaJson.r42) {
       $("#area_25").attr("fill","red")
   } else {
       $("#area_25").attr("fill","#49c419")
   } 
   if (areaJson.r43 || areaJson.r44){
       $("#area_23").attr("fill","red")
   } else {
       $("#area_23").attr("fill","#49c419")
   } 
   if (areaJson.r45 || areaJson.r46){
       $("#area_21").attr("fill","red")
   } else {
       $("#area_21").attr("fill","#49c419")
   } 
   if (areaJson.r47){
       $("#area_18").attr("fill","red")
   } else {
       $("#area_18").attr("fill","#49c419")
   } 
   if (areaJson.r48){
       $("#area_15").attr("fill","red")
   } else {
       $("#area_15").attr("fill","#49c419")
   } 
   if (areaJson.r49 || areaJson.r50){
       $("#area_13").attr("fill","red")
   } else {
       $("#area_13").attr("fill","#49c419")
   } 
   if (areaJson.r51 || areaJson.r52){
       $("#area_11").attr("fill","red")
   } else {
       $("#area_11").attr("fill","#49c419")
   } 
   if (areaJson.r53 || areaJson.r54 || areaJson.r55){
       $("#area_9").attr("fill","red")
   } else {
       $("#area_9").attr("fill","#49c419")
   } 
   if (areaJson.r56 || areaJson.r57 || areaJson.r58){
       $("#area_7").attr("fill","red")
   } else {
       $("#area_7").attr("fill","#49c419")
   } 
   if (areaJson.r59){
       $("#area_5").attr("fill","red")
   } else {
       $("#area_5").attr("fill","#49c419")
   } 
   if (areaJson.r60 || areaJson.r61 | areaJson.r62){
       $("#area_3").attr("fill","red")
   } else {
       $("#area_3").attr("fill","#49c419")
   } 
   if (areaJson.r63 || areaJson.r64 | areaJson.r65){
       $("#area_1").attr("fill","red")
   } else {
       $("#area_1").attr("fill","#49c419")
   }  
}
function showErrorMsg(areaJson){
    let westError = 0;
    let eastError = 0;
    let roadError = 0;

    //West
    if (areaJson.r18 || areaJson.r19 || areaJson.r20 || areaJson.r21 || areaJson.r22 || areaJson.r23 || areaJson.r24 || areaJson.r25 || areaJson.r26
        || areaJson.r27 || areaJson.r28 || areaJson.r29 || areaJson.r30 || areaJson.r31 || areaJson.r32 || areaJson.r33 || areaJson.r34 || areaJson.r35
        || areaJson.r36 || areaJson.r37 || areaJson.r38 || areaJson.r39 || areaJson.r40 || areaJson.r41 || areaJson.r42 || areaJson.r43 || areaJson.r44
        || areaJson.r45 || areaJson.r46 || areaJson.r47 ) {
        westError = 1;
    }
    //East
    if (areaJson.r1 || areaJson.r2 || areaJson.r3 || areaJson.r4 || areaJson.r5 || areaJson.r6 || areaJson.r7 || areaJson.r8 || areaJson.r9
        || areaJson.r56 || areaJson.r57 || areaJson.r58 || areaJson.r59 || areaJson.r60 || areaJson.r61 || areaJson.r62 || areaJson.r63 || areaJson.r64 || areaJson.r65) {
        eastError = 1;
    } 
    if (areaJson.r10 || areaJson.r11 || areaJson.r12 || areaJson.r13 || areaJson.r14 || areaJson.r15 || areaJson.r16 || areaJson.r17
        || areaJson.r48 || areaJson.r49 || areaJson.r50 || areaJson.r51 || areaJson.r52 || areaJson.r53 || areaJson.r54 || areaJson.r55) {
        roadError =1;
    } 
    if (roadError || eastError || westError){
        playNotificationSound();
        errorMode();      
        if (westError==1)
            $(".west-error-msg").css("display", "block");
        else
            $(".west-error-msg").css("display", "none");

        if (roadError == 1)
            $(".road-error-msg").css("display", "block");
        else
            $(".road-error-msg").css("display", "none");

        if (eastError == 1)
            $(".east-error-msg").css("display", "block");
        else
            $(".east-error-msg").css("display", "none");
    }else{
        $(".west-error-msg").css("display", "none");
        $(".road-error-msg").css("display", "none");
        $(".east-error-msg").css("display", "none");
        normalMode();
    }
}
function playNotificationSound(){
    var audio = new Audio('/sound/notification.mp3');
    audio.play();
}
function getLRTData() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    date = yyyy + "" + mm + "" + dd;
    var starCountRef = firebase.database().ref(date + '/restriction').orderByChild('time').limitToLast(1);
    starCountRef.on('value', function (snapshot) {
        console.log(snapshot.val())
        var dbRes = snapshot.val();
        for (data in dbRes) {
            showAlarmOnArea(dbRes[data]);
            showErrorMsg(dbRes[data]);
            
        }
    });
}

function normalMode(){
    $(".east-title").css("display", "inline");
    $(".west-title").css("display", "inline");
    $("#msg").css("display", "none");
    $("#map").css("width", "100%");
    $("svg").css("transform", "scale(1.7,1)");
    $("#map").css("display", "block");
    $("body").css("background-color", "rgb(251, 221, 221)");

}
function errorMode(){
    $(".east-title").css("display", "none");
    $(".west-title").css("display", "none");
    $("#map").css("display", "block");
    $("#msg").css("display", "inline-flex");
    $("#map").css("width", "66.66666667%");
    $("svg").css("transform","scale(1,1)");
    $("svg").css("width","100%");
    $("body").css("background-color", "rgb(251, 221, 221)");


}

$(function () {
    var firebaseConfig = {
        apiKey: "AIzaSyDdnhBWmyStk3DgV_l6y_iWaMrBQKAvT18",
        authDomain: "kaohsiung-lrt.firebaseapp.com",
        databaseURL: "https://kaohsiung-lrt.firebaseio.com",
        projectId: "kaohsiung-lrt",
        storageBucket: "kaohsiung-lrt.appspot.com",
        messagingSenderId: "550813548509",
        appId: "1:550813548509:web:041a79ede4881ab175c034",
        measurementId: "G-WRWZL1XRR7"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    var database = firebase.database();
    getLRTData(); 
  //  errorMode()
});
