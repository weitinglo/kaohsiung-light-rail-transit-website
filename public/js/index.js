var app = angular.module('myApp', []);
app.controller('myCtrl', function ($scope) {
    $scope.totalIntrudeCount = 0;
    $scope.totalDataCount = 0;
    $scope.tableData=[];
    $scope.scheduleData = [];

    $scope.lineChartData=[];
    $scope.barChartData = [];

    $scope.areaIntrudeMost ="無";

    function plotLineChart(lineChartData1) {
        var line_data1 = {
            data: lineChartData1,
            color: '#3c8dbc'
        }
        $.plot('#line-chart', [line_data1], {
            grid: {
                borderColor: '#f3f3f3',
                borderWidth: 1,
                tickColor: '#f3f3f3',
                hoverable: true,
            },
            series: {
                shadowSize: 0,
                lines: {
                    show: true
                },
                points: {
                    show: true
                }
            },
            lines: {
                fill: false,
                color: ['#3c8dbc', '#f56954']
            },
            yaxis: {
                show: true,
            },
            xaxis: {
                ticks: [
                    [0, '0 AM'], [1, '1 AM'], [2, '2 AM'], [3, '3 AM'], [4, '4 AM'], [5, '5 AM'], [6, '6 AM'], [7, '7 AM'], [8, '8 AM'], [9, '9 AM'],
                    [10, '10 AM'], [11, '11 AM'], [12, '12 PM'], [13, '1 PM'], [14, '2 PM'], [15, '3 PM'], [16, '4 PM'], [17, '5 PM'], [18, '6 PM'], [19, '7 PM'],
                    [20, '8 PM'], [21, '9 PM'], [22, '10 PM'], [23, '11 PM'], [24, '12 AM']
                ]
            }
        })
    }
    //Initialize tooltip on hover
    $('<div class="tooltip-inner" id="line-chart-tooltip"></div>').css({
        position: 'absolute',
        display: 'none',
        opacity: 0.8
    }).appendTo('body')
    $('#line-chart').bind('plothover', function (event, pos, item) {
        if (item) {
            let count = $scope.lineChartData[item.dataIndex][1];
            let hour = $scope.lineChartData[item.dataIndex][0];

            $('#line-chart-tooltip').html(count + ' 次 在 ' + hour%12 +' 點')
                .css({
                    top: item.pageY + 5,
                    left: item.pageX + 5
                })
                .fadeIn(200)
        } else {
            $('#line-chart-tooltip').hide()
        }
    })


    function plotBarChart(tableData) {
        var areaData = [];
        $scope.barChartData=[];
        var count = 0;
        for (i in tableData) {            
            areaData.push([count++, tableData[i]]);
        }
        let theMost = 0;
        for(i in areaData){
            $scope.barChartData.push([areaData[i][0] + 1, areaData[i][1]]);
            console.log(areaData[i])
            if (areaData[i][1]>theMost){
                theMost = areaData[i][1];
                $scope.areaIntrudeMost = "區域" + (parseInt(areaData[i][0])+1);
            }
        }
        var bar_data = {
            data: areaData,
            bars: { show: true }
        }
        $.plot('#bar-chart', [bar_data], {
            grid: {
                borderWidth: 1,
                borderColor: '#f3f3f3',
                tickColor: '#f3f3f3',
                hoverable: true
            },
            series: {
                bars: {
                    show: true, barWidth: 0.5, align: 'center',
                },
            },
            colors: ['#3c8dbc'],
            xaxis: {
                ticks: [
                    [0, '區域1'], [1, '區域2'], [2, '區域3'], [3, '區域4'], [4, '區域5'], [5, '區域6'], [6, '區域7'], [7, '區域8'], [8, '區域9'], [9, '區域10'], 
                    [10, '區域11'], [11, '區域12'], [12, '區域13'], [13, '區域14'], [14, '區域15'], [15, '區域16'], [16, '區域18'], [17, '區域18'], [18, '區域19'], [19, '區域20'],
                    [20, '區域21'], [21, '區域22'], [22, '區域23'], [23, '區域24'], [24, '區域25'], [25, '區域26']
                ]
            }
        })
    }
    //Initialize tooltip on hover
    $('<div class="tooltip-inner" id="bar-chart-tooltip"></div>').css({
        position: 'absolute',
        display: 'none',
        opacity: 0.8
    }).appendTo('body')
    $('#bar-chart').bind('plothover', function (event, pos, item) {
        if (item) {
            let count = $scope.barChartData[item.dataIndex][1];
            let area = $scope.barChartData[item.dataIndex][0];

            $('#bar-chart-tooltip').html(count + ' 次在區域 ' + area)
                .css({
                    top: item.pageY + 5,
                    left: item.pageX + 5
                })
                .fadeIn(200)
        } else {
            $('#bar-chart-tooltip').hide()
        }
    })
    Object.size = function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };
    function plotDonutChart(tableData) {
        let donutData=[];
        for (i in tableData) {
            var json = {
                label: "闖入 " + i,
                data: tableData[i]
            }
            donutData.push(json)
        }
        var okAreaJson = {
            label: '沒被闖入',
            data: $scope.totalDataCount * Object.size(tableData),
            color: '#3c8dbc'
        }
        donutData.push(okAreaJson);
        $.plot('#donut-chart', donutData, {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                    innerRadius: 0.5,
                    label: {
                        show: true,
                        radius: 2 / 3,
                        formatter: labelFormatter,
                        threshold: 0.1
                    }
                }
            },
            legend: {
                show: false
            }
        })
    }

    function areaSumFilter(areaJson){
        let tableData = { "area1": 0, "area2": 0, "area3": 0, "area4": 0, "area5": 0, "area6": 0, "area7": 0, "area8": 0, "area9": 0, "area10": 0, "area11": 0, "area12": 0, "area13": 0,
            "area14": 0, "area15": 0, "area16": 0, "area17": 0, "area18": 0, "area19": 0, "area20": 0, "area21": 0, "area22": 0, "area23": 0, "area24": 0, "area25": 0, "area26": 0};     
        /*C13*/
        if (areaJson.r1 || areaJson.r2 || areaJson.r3)
            tableData["area2"] = Math.max(areaJson.r1,areaJson.r2,areaJson.r3);   
        if (areaJson.r4 || areaJson.r5 || areaJson.r6)
            tableData["area4"] = Math.max(areaJson.r4,areaJson.r5,areaJson.r6);   
        if (areaJson.r7)
            tableData["area6"] = areaJson.r7;  
        if (areaJson.r8 || areaJson.r9)
            tableData["area8"] = Math.max(areaJson.r8,areaJson.r9);   
        if (areaJson.r10 || areaJson.r11 || areaJson.r12) 
            tableData["area10"] = Math.max(areaJson.r10,areaJson.r11,areaJson.r12);   
        if (areaJson.r13 || areaJson.r14) 
            tableData["area12"] = Math.max(areaJson.r13,areaJson.r14); 
        if (areaJson.r15 || areaJson.r16) 
            tableData["area14"] = Math.max(areaJson.r15,areaJson.r16);   
        if (areaJson.r17 || areaJson.r18) 
            tableData["area16"] = Math.max(areaJson.r17,areaJson.r18);   
        if (areaJson.r19 || areaJson.r20 || areaJson.r21 || areaJson.r22 || areaJson.r23 || areaJson.r24 || areaJson.r25 || areaJson.r26 || areaJson.r27 || areaJson.r28 || areaJson.r29 || areaJson.r30 || areaJson.r31 || areaJson.r32 || areaJson.r33) 
            tableData["area17"] = Math.max(areaJson.r19,areaJson.r20,areaJson.r21,areaJson.r22,areaJson.r23,areaJson.r24,areaJson.r25,areaJson.r26,areaJson.r27,areaJson.r28,areaJson.r29,areaJson.r30,areaJson.r31,areaJson.r32,areaJson.r33);    
        if (areaJson.r34 || areaJson.r35) 
            tableData["area19"] = Math.max(areaJson.r34,areaJson.r35);   
        if (areaJson.r36)
            tableData["area20"] = areaJson.r20;  
        if (areaJson.r37 || areaJson.r38) 
            tableData["area22"] = Math.max(areaJson.r37,areaJson.r38);   
        if (areaJson.r39 || areaJson.r40) 
            tableData["area24"] = Math.max(areaJson.r39,areaJson.r40);  
        if (areaJson.r41) 
            tableData["area26"] = areaJson.r41;  

        /*C14*/
        if (areaJson.r42)
            tableData["area25"] = areaJson.r42;   
        if (areaJson.r43 || areaJson.r44)
            tableData["area23"] = Math.max(areaJson.r43,areaJson.r44);   
        if (areaJson.r45 || areaJson.r46)
            tableData["area21"] = Math.max(areaJson.r45,areaJson.r46);  
        if (areaJson.r47)
            tableData["area18"] = areaJson.r47;  
        if (areaJson.r48)
            tableData["area15"] = areaJson.r48; 
        if (areaJson.r49 || areaJson.r50)
            tableData["area13"] = Math.max(areaJson.r49,areaJson.r50);   
        if (areaJson.r51 || areaJson.r52)
            tableData["area11"] = Math.max(areaJson.r51,areaJson.r52);   
        if (areaJson.r53 || areaJson.r54 || areaJson.r55)
            tableData["area9"] = Math.max(areaJson.r53,areaJson.r54,areaJson.r55);
        if (areaJson.r56 || areaJson.r57 || areaJson.r58)
            tableData["area7"] = Math.max(areaJson.r56,areaJson.r57,areaJson.r58);
        if (areaJson.r59)
            tableData["area5"] = areaJson.r59; 
        if (areaJson.r60 || areaJson.r61 | areaJson.r62)
            tableData["area3"] = Math.max(areaJson.r60,areaJson.r61,areaJson.r62);      
        if (areaJson.r63 || areaJson.r64 | areaJson.r65)
            tableData["area1"] = Math.max(areaJson.r63,areaJson.r64,areaJson.r65);      
            
        return tableData;

    }
    function plotTable(dbData){
        for(key in dbData){
            if (dbData[key]){
                let tableData = areaSumFilter(dbData[key].data);
                tableData.time = key + " 時";
                $scope.tableData.push(tableData);
            }
            
        }
        $scope.$apply();
    }
    function getLRTData(date) {
        $.ajax({
            url: 'lrtData?date=' + date,
            type: 'GET',
            error: function () {
                alert("error");
            },
            success: function (dbData) {
                
                if (dbData.code != 101) {
                    $scope.lineChartData = [];
                    $scope.totalDataCount=0;
                    $scope.totalIntrudeCount=0;
                    var barChartData ={};
                    for(var i=0;i<24;i++){
                        $scope.lineChartData.push([i,0]);
                    }
                    for(key in dbData){
                        var dbDataJson = dbData[key];
                        if (dbDataJson){
                            if (dbDataJson['dataCount'])
                                $scope.totalDataCount += parseInt(dbDataJson['dataCount']);
                            if (dbDataJson['intrudeCount'])
                                $scope.totalIntrudeCount += parseInt(dbDataJson['intrudeCount']);
                            let areaData = dbDataJson["data"];
                            $scope.lineChartData[parseInt(key) - 1] = ([parseInt(key), parseInt(dbDataJson['intrudeCount'])]);
                            for (area in areaData) {
                                if (typeof barChartData[area] === 'undefined') {
                                    barChartData[area] = areaData[area];
                                } else {
                                    barChartData[area] += areaData[area];
                                }
                            }
                        }
                        
                    }
                    $scope.$apply();
                    let tableData = areaSumFilter(barChartData);  
                    console.log(tableData);            
                    // plot charts
                    plotLineChart($scope.lineChartData);
                    plotBarChart(tableData)
                    plotDonutChart(tableData);
                    plotTable(dbData);
                  
                }
            }
        });
    }
    function getLRTSchedule(date) {
        $.ajax({
            url: 'lrtSchedule?date=' + date,
            type: 'GET',
            error: function () {
                alert("error");
            },
            success: function (dbData) {
                if (dbData.code != 101) {
                    $scope.scheduleData.length=0;
                    for (key in dbData) {
                        var dbDataJson = dbData[key];
                        if(dbDataJson['A']!=""){
                            let data={
                                type: "C14 哈瑪星 => C13 駁二蓬萊",
                                west : dbDataJson['B1'],
                                east : dbDataJson['C1'],
                                direction: ">>"
                            };
                            $scope.scheduleData.push(data);
                        }
                        if (dbDataJson['D'] != "") {
                            let data = {
                                type: "C13 駁二蓬萊 => C14 哈瑪星",
                                west: dbDataJson['B2'],
                                east: dbDataJson['C2'],
                                direction: "<<"
                            };
                            $scope.scheduleData.push(data);
                        }
                    } 
                    $scope.$apply();
                }
            }
        });
    }
    getLRTData(getCurrentDate());
    getLRTSchedule(getCurrentDate());

    /* END LINE CHART */

});
function getCurrentDate() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    return yyyy + "" + mm + "" + dd;
}
/*
 * Custom Label formatter
 * ----------------------
 */
function labelFormatter(label, series) {
    return '<div style="font-size:12px; text-align:center; color: #fff; font-weight: 600;">'
        + label
        + '<br>'
        + Math.round(series.percent) + '%</div>'
}
