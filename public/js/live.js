
var app = angular.module('myApp', []);
app.controller('myCtrl', function ($scope, $timeout) {
    $scope.statusA = "";
    $scope.statusB = "";
    $scope.statusC = "";
    $scope.statusD = ""
    $scope.c13Countdown=0;
    $scope.c14Countdown = 0;
    $scope.data={};
    $scope.clock = "loading clock..."; // initialise the time variable
    $scope.tickInterval = 1000 //ms
    var firebaseConfig = {
        apiKey: "AIzaSyDdnhBWmyStk3DgV_l6y_iWaMrBQKAvT18",
        authDomain: "kaohsiung-lrt.firebaseapp.com",
        databaseURL: "https://kaohsiung-lrt.firebaseio.com",
        projectId: "kaohsiung-lrt",
        storageBucket: "kaohsiung-lrt.appspot.com",
        messagingSenderId: "550813548509",
        appId: "1:550813548509:web:041a79ede4881ab175c034",
        measurementId: "G-WRWZL1XRR7"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    var tick = function () {
        $scope.clock = Date.now() // get the current time
        $timeout(tick, $scope.tickInterval); // reset the timer
    }
    // Start the timer
    $timeout(tick, $scope.tickInterval);
    $(".clock").css("display", "block");
    var database = firebase.database();
    function showAlarmOnArea(areaJson) {
         /*C13*/
        if (areaJson.r1 || areaJson.r2 || areaJson.r3){
            $("#area_2").attr("fill","red")
        } else {
            $("#area_2").attr("fill","#49c419")
        }
        if (areaJson.r4 || areaJson.r5 || areaJson.r6){
            $("#area_4").attr("fill","red")
        } else {
            $("#area_4").attr("fill","#49c419")
        } 
        if (areaJson.r7){
            $("#area_6").attr("fill","red")
        } else {
            $("#area_6").attr("fill","#49c419")
        } 
        if (areaJson.r8 || areaJson.r9){
            $("#area_8").attr("fill","red")
        } else {
            $("#area_8").attr("fill","#49c419")
        } 
        if (areaJson.r10 || areaJson.r11 || areaJson.r12){
            $("#area_10").attr("fill","red")
        } else {
            $("#area_10").attr("fill","#49c419")
        } 
        if (areaJson.r13 || areaJson.r14) {
            $("#area_12").attr("fill","red")
        } else {
            $("#area_12").attr("fill","#49c419")
        } 
        if (areaJson.r15 || areaJson.r16) {
            $("#area_14").attr("fill","red")
        } else {
            $("#area_14").attr("fill","#49c419")
        } 
        if (areaJson.r17 || areaJson.r18) {
            $("#area_16").attr("fill","red")
        } else {
            $("#area_16").attr("fill","#49c419")
        } 
        if (areaJson.r19 || areaJson.r20 || areaJson.r21 || areaJson.r22 || areaJson.r23 || areaJson.r24 || areaJson.r25 || areaJson.r26 || areaJson.r27 || areaJson.r28 || areaJson.r29 || areaJson.r30 || areaJson.r31 || areaJson.r32 || areaJson.r33) {
            $("#area_17").attr("fill","red")
        } else {
            $("#area_17").attr("fill","#49c419")
        } 
        if (areaJson.r34 || areaJson.r35) {
            $("#area_19").attr("fill","red")
        } else {
            $("#area_19").attr("fill","#49c419")
        } 
        if (areaJson.r36) {
            $("#area_20").attr("fill","red")
        } else {
            $("#area_20").attr("fill","#49c419")
        } 
        if (areaJson.r37 || areaJson.r38) {
            $("#area_22").attr("fill","red")
        } else {
            $("#area_22").attr("fill","#49c419")
        } 
        if (areaJson.r39 || areaJson.r40) {
            $("#area_24").attr("fill","red")
        } else {
            $("#area_24").attr("fill","#49c419")
        } 
        if (areaJson.r41) {
            $("#area_26").attr("fill","red")
        } else {
            $("#area_26").attr("fill","#49c419")
        } 

     /*C14*/
        if (areaJson.r42) {
            $("#area_25").attr("fill","red")
        } else {
            $("#area_25").attr("fill","#49c419")
        } 
        if (areaJson.r43 || areaJson.r44){
            $("#area_23").attr("fill","red")
        } else {
            $("#area_23").attr("fill","#49c419")
        } 
        if (areaJson.r45 || areaJson.r46){
            $("#area_21").attr("fill","red")
        } else {
            $("#area_21").attr("fill","#49c419")
        } 
        if (areaJson.r47){
            $("#area_18").attr("fill","red")
        } else {
            $("#area_18").attr("fill","#49c419")
        } 
        if (areaJson.r48){
            $("#area_15").attr("fill","red")
        } else {
            $("#area_15").attr("fill","#49c419")
        } 
        if (areaJson.r49 || areaJson.r50){
            $("#area_13").attr("fill","red")
        } else {
            $("#area_13").attr("fill","#49c419")
        } 
        if (areaJson.r51 || areaJson.r52){
            $("#area_11").attr("fill","red")
        } else {
            $("#area_11").attr("fill","#49c419")
        } 
        if (areaJson.r53 || areaJson.r54 || areaJson.r55){
            $("#area_9").attr("fill","red")
        } else {
            $("#area_9").attr("fill","#49c419")
        } 
        if (areaJson.r56 || areaJson.r57 || areaJson.r58){
            $("#area_7").attr("fill","red")
        } else {
            $("#area_7").attr("fill","#49c419")
        } 
        if (areaJson.r59){
            $("#area_5").attr("fill","red")
        } else {
            $("#area_5").attr("fill","#49c419")
        } 
        if (areaJson.r60 || areaJson.r61 | areaJson.r62){
            $("#area_3").attr("fill","red")
        } else {
            $("#area_3").attr("fill","#49c419")
        } 
        if (areaJson.r63 || areaJson.r64 | areaJson.r65){
            $("#area_1").attr("fill","red")
        } else {
            $("#area_1").attr("fill","#49c419")
        }  
    }
    function setIntervalX(callback, delay, repetitions) {
        var x = 0;
        var intervalID = window.setInterval(function () {
            callback();
            if (++x === repetitions) {
                window.clearInterval(intervalID);
            }
        }, delay);
    }
    function getLRTData() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        date = yyyy + "" + mm + "" + dd;
        var starCountRef = firebase.database().ref(date + '/restriction').orderByChild('time').limitToLast(1);
        starCountRef.on('value', function (snapshot) {
            console.log(snapshot.val())
            var dbRes = snapshot.val();
            for(data in dbRes){
                showAlarmOnArea(dbRes[data]);
            }
        });
    }
    function getLRTStatus() {
        var ref = firebase.database().ref('/status');
        ref.on('value', function (snapshot) {
           // console.log(snapshot.val())
            let dataObj = snapshot.val();
            $scope.data = dataObj;
            if (dataObj["A"] == 1 && dataObj["B1"] != 1 && dataObj["C1"] != 1) { //A
                $scope.c14Countdown = 1;
                $("#train-AB").css("display", "inline");
                $("#train-BC").css("display", "none");
                $("#train-CD").css("display", "none");
                $("#ADProgress").css("width", "30%");
                $("#ADProgress").text("從哈瑪星站往蓬萊路口");
            } else if (dataObj["B1"] == 1 && dataObj["C1"] != 1){ // A+B1 or B1
                $("#train-AB").css("display", "none");
                $("#train-BC").css("display", "inline");
                $("#train-CD").css("display", "none");
                $("#ADProgress").css("width", "60%");
                $("#ADProgress").text("蓬萊路口");
                $scope.c14Countdown = 1;
            } else if (dataObj["C1"] == 1) { // A+B1+C1 or A+C1 or B1+C1 or C1
                $("#train-AB").css("display", "none");
                $("#train-BC").css("display", "none");
                $("#train-CD").css("display", "inline");
                $("#ADProgress").css("width", "94%");
                $("#ADProgress").text("蓬萊路口往駁二蓬萊站");
                setIntervalX(function () {
                    $("#train-AB").css("display", "none");
                    $("#train-BC").css("display", "none");
                    $("#train-CD").css("display", "none");
                    $("#ADProgress").css("width", "0%");
                    $("#ADProgress").text("");
                }, 10000, 1);
            } 

            if (dataObj["D"] == 1 && dataObj["C2"] != 1 && dataObj["B2"] != 1) { // D
                $("#train-DC").css("display", "inline");
                $("#train-CB").css("display", "none");
                $("#train-BA").css("display", "none");
                $("#DAProgress").css("width", "30%");
                $("#DAProgress").text("從駁二蓬萊站往蓬萊路口");
                $scope.c13Countdown = 1;
            } else if (dataObj["C2"] == 1 && dataObj["B2"] != 1) { // D+C2 or C2
                $("#train-DC").css("display", "none");
                $("#train-CB").css("display", "inline");
                $("#train-BA").css("display", "none");
                $("#DAProgress").css("width", "60%");
                $("#DAProgress").text("蓬萊路口");
                $scope.c13Countdown = 1;
            } else if (dataObj["B2"] == 1) { // D+C2+B2 or C2+B2 or B2
                $("#train-DC").css("display", "none");
                $("#train-CB").css("display", "none");
                $("#train-BA").css("display", "inline");
                $("#DAProgress").css("width", "94%");
                $("#DAProgress").text("蓬萊路口往駁二蓬萊站");
                setIntervalX(function () {
                    $("#train-DC").css("display", "none");
                    $("#train-CB").css("display", "none");
                    $("#train-BA").css("display", "none");
                    $("#DAProgress").css("width", "0%");
                    $("#DAProgress").text("");
                }, 20000, 1);
            } 
        });
    }
   getLRTData(); 
   getLRTStatus();
});

function resizeMap(){
    var $el = $("#detect-map-block");
    var elHeight = $el.outerHeight();
    var elWidth = $el.outerWidth();
    var $imgel = $("#detect-map-img");
    var imgelHeight = $imgel.outerHeight();
    var imgelWidth = $imgel.outerWidth();
   
    var scale = elWidth / imgelWidth;
    $("#detect-map").css({
        transform: "scale(" + scale + ")"
    });
}

$(function () {
    // var canvas1 = document.getElementById('video-canvas1');
    // var url1 = 'ws://' + document.location.hostname + ':9999/';
    // var player1 = new JSMpeg.Player(url1, { canvas: canvas1 });
    // var canvas2 = document.getElementById('video-canvas2');
    // var url2 = 'ws://' + document.location.hostname + ':8888/';
    // var player2 = new JSMpeg.Player(url2, { canvas: canvas2 });
    // resizeMap();
   

});
$(window).resize(function () {
    resizeMap();
});
